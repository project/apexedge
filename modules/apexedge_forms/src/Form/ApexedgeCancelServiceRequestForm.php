<?php

namespace Drupal\apexedge_forms\Form;

use Drupal\apexedge\Services\ServiceRequestService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Service request form.
 */
class ApexedgeCancelServiceRequestForm extends FormBase {

  /**
   * The service request service.
   *
   * @var \Drupal\apexedge\Services\ServiceRequestService
   */
  protected $serviceRequestService;

  /**
   * The Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * CancelBillForm constructor.
   */
  public function __construct(
    ServiceRequestService $service_request_service,
    LoggerChannelFactoryInterface $factory,
    MessengerInterface $messenger
  ) {
    $this->serviceRequestService = $service_request_service;
    $this->loggerFactory = $factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('apexedge.service_request_service'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apexedge_cancel_service_request';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $configs = []) {
    $form['#prefix'] = '<div id="apex-edge-cancel-service-form">';
    $form['#suffix'] = '</div>';

    $form['success_message'] = [
      '#type' => 'hidden',
      '#value' => $configs['success_message'] ?? '',
    ];

    $form['service_request_id'] = [
      '#type' => isset($configs['service_request_id']) && $configs['service_request_id'] ? 'hidden' : 'textfield',
      '#title' => $this->t('Service Request ID'),
      '#required' => TRUE,
      '#default' => $configs['service_request_id'] ?? $form_state->getValue('service_request_id'),
    ];

    $form['customer_id'] = [
      '#type' => isset($configs['customer_id']) && $configs['customer_id'] ? 'hidden' : 'textfield',
      '#title' => $this->t('Customer ID'),
      '#required' => TRUE,
      '#default' => $configs['customer_id'] ?? $form_state->getValue('customer_id'),
    ];

    $form['cancel'] = [
      '#type' => 'submit',
      '#value' => $configs['cancel_service_button_text'] ?? $this->t('Cancel service request'),
      '#attributes' => [
        'class' => [
          'btn-primary',
          'ms-0',
          'd-block',
          'w-100',
        ],
      ],
      '#ajax' => [
        'callback' => '::submit',
        'wrapper' => 'apex-edge-cancel-service-form',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Submits the cancel service form via ajax.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $service_request_id = $form_state->getValue('service_request_id');
    $customer_id = $form_state->getValue('customer_id');
    if ($service_request_id && $customer_id) {
      $this->serviceRequestService->cancelServiceRequest($customer_id, $service_request_id);
      $this->loggerFactory->get('apexedge_forms')
        ->notice('The Service request (id = ' . $service_request_id . ') canceled.');

      $response = new AjaxResponse();
      $response->addCommand(
        new ReplaceCommand(
          '#apex-edge-cancel-service-form',
          Markup::create($form_state->getValue('success_message'))->__toString()
        )
      );
      return $response;
    }
  }

}
