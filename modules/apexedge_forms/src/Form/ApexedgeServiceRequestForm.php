<?php

namespace Drupal\apexedge_forms\Form;

use Apexedge\DTO\ServiceRequest\CustomerAccountParams;
use Apexedge\DTO\ServiceRequest\ServiceRequestParams;
use Drupal\apexedge\Services\ServiceRequestService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Service request form.
 */
class ApexedgeServiceRequestForm extends FormBase {

  /**
   * The service request service.
   *
   * @var \Drupal\apexedge\Services\ServiceRequestService
   */
  protected $serviceRequestService;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * CancelBillForm constructor.
   */
  public function __construct(
    ServiceRequestService $service_request_service,
    UserStorageInterface $user_storage,
    AccountProxyInterface $current_user,
    LoggerChannelFactoryInterface $factory,
    MessengerInterface $messenger
  ) {
    $this->serviceRequestService = $service_request_service;
    $this->userStorage = $user_storage;
    $this->currentUser = $current_user;
    $this->loggerFactory = $factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('apexedge.service_request_service'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apexedge_service_request';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $configs = []) {
    $user = $this->userStorage->load($this->currentUser->id());

    $form['#prefix'] = '<div id="apex-edge-service-request-form">';
    $form['#suffix'] = '</div>';

    $form['confirmation_message'] = [
      '#type' => 'hidden',
      '#value' => $configs['confirmation_message'] ?? '',
    ];

    $form['type'] = [
      '#type' => 'hidden',
      '#value' => $configs['type'] ?? '',
    ];

    $form['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $configs['title'] ?? '',
      '#attributes' => [
        'class' => [
          'body',
          'loga-palette-1',
          'text-center',
        ],
      ],
    ];

    $form['body'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $configs['body'] ?? '',
      '#attributes' => [
        'class' => [
          'body',
          'loga-palette-1',
        ],
      ],
    ];

    $form['name-container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'row',
        ],
      ],
    ];

    $form['name-container']['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
      '#required' => TRUE,
      '#default_value' => $user->field_first_name->value ?? '',
      '#wrapper_attributes' => [
        'class' => [
          'col-12',
          'col-md-6',
        ],
      ],
    ];

    $form['name-container']['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#required' => TRUE,
      '#default_value' => $user->field_last_name->value ?? '',
      '#wrapper_attributes' => [
        'class' => [
          'col-12',
          'col-md-6',
        ],
      ],
    ];

    $form['contacts_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'row',
        ],
      ],
    ];

    $form['contacts_container']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#default_value' => $user->mail->value ?? '',
      '#wrapper_attributes' => [
        'class' => [
          'col-12',
          'col-md-6',
        ],
      ],
    ];

    $form['contacts_container']['contact_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact number'),
      '#required' => TRUE,
      '#default_value' => $user->field_cell_number->value ?? '',
      '#wrapper_attributes' => [
        'class' => [
          'col-12',
          'col-md-6',
        ],
      ],
    ];

    $form['service_provider'] = [
      '#type' => 'apexedge_billers',
      '#title' => $this->t('Service Provider'),
      '#required' => TRUE,
    ];

    $form['average_monthly_payment'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#title' => $this->t('Average monthly payment'),
      '#required' => TRUE,
    ];

    $form['create'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
      '#attributes' => [
        'class' => [
          'btn-primary',
          'ms-0',
          'd-block',
          'w-100',
        ],
      ],
      '#ajax' => [
        'callback' => '::submit',
        'wrapper' => 'apex-edge-service-request-form',
      ],
    ];

    return $form;
  }

  /**
   * Submits the cancel service form via ajax.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    if ($form_state->getErrors()) {
      return $form;
    }
    $response = new AjaxResponse();
    $account_params = new CustomerAccountParams(
      first_name: $form_state->getValue('first_name'),
      last_name: $form_state->getValue('last_name'),
      email: $form_state->getValue('email'),
      contact_number: preg_replace('/[^0-9.]+/', '', $form_state->getValue('contact_number')),
    );
    $customer_id = $this->serviceRequestService->createCustomerAccount($account_params);
    if ($customer_id) {
      $service_request_params = new ServiceRequestParams(
        type: $form_state->getValue('type'),
        biller_id: $form_state->getValue('service_provider'),
        original_monthly_amount: $form_state->getValue('average_monthly_payment'),
      );

      $service_request = $this->serviceRequestService->createServiceRequest($customer_id, $service_request_params);
      if ($service_request->id) {
        $this->loggerFactory->get('apexedge_forms')
          ->notice('The Service request (id = ' . $service_request->id . ') created.');

        $response = new AjaxResponse();
        $response->addCommand(
          new ReplaceCommand(
            '#apex-edge-service-request-form',
            Markup::create($form_state->getValue('confirmation_message'))->__toString()
          )
        );
      }
      return $response;
    }
    $response->addCommand(
      new ReplaceCommand(
        '#apex-edge-service-request-form',
        Markup::create('Unable to process your request. Please try again later'),
      )
    );
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}/', $form_state->getValue('contact_number'))) {
      $form_state->setError($form['contacts_container']['contact_number'], $this->t('Invalid contact number.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
