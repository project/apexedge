<?php

namespace Drupal\apexedge_forms\Form;

use Apexedge\DTO\ServiceRequest\MissingDataParam;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\apexedge\Services\ApexEdgeService;
use Drupal\apexedge\Services\ServiceRequestService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Missing data form.
 */
class ApexedgeMissingDataForm extends FormBase {

  /**
   * The service request service.
   *
   * @var \Drupal\apexedge\Services\ServiceRequestService
   */
  protected ServiceRequestService $serviceRequestService;

  /**
   * The Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ApexEdge service.
   *
   * @var \Drupal\apexedge\Services\ApexEdgeService
   */
  protected ApexEdgeService $apexEdgeService;

  /**
   * CancelBillForm constructor.
   */
  public function __construct(
    ServiceRequestService $service_request_service,
    LoggerChannelFactoryInterface $factory,
    MessengerInterface $messenger,
    ApexEdgeService $apex_edge_service
  ) {
    $this->serviceRequestService = $service_request_service;
    $this->loggerFactory = $factory;
    $this->messenger = $messenger;
    $this->apexEdgeService = $apex_edge_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ApexedgeMissingDataForm|static {
    return new static(
      $container->get('apexedge.service_request_service'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('apexedge.apexedge_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'apexedge_cancel_service_request';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $configs = []): array {
    $form['#prefix'] = '<div id="apex-edge-missing-data-form">';
    $form['#suffix'] = '</div>';

    $form['success_message'] = [
      '#type' => 'hidden',
      '#value' => $configs['success_message'] ?? '',
    ];

    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $service_request = $apexEdge->getServiceRequest($configs['customer_id'], $configs['service_request_id']);
    }
    catch (InvalidApexedgeRequest $e) {
      $this->apexEdgeService->logApiError($e->getResponse());
    }

    if (empty($service_request->missing_data)) {
      $form['markup'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No missing data in this service request.'),
        '#attributes' => [
          'class' => [
            'text-center',
            'my-3',
            'text-loga-palette-1',
          ],
        ],
      ];

      return $form;
    }

    $form_state->setStorage(['missing_data' => $service_request->missing_data]);

    foreach ($service_request->missing_data as $missing_data_item) {
      $form[$missing_data_item->field] = [
        '#type' => 'textfield',
        '#title' => $missing_data_item->question,
        '#required' => TRUE,
      ];
    }

    $form['service_request_id'] = [
      '#type' => 'hidden',
      '#value' => $configs['service_request_id'],
    ];

    $form['customer_id'] = [
      '#type' => 'hidden',
      '#value' => $configs['customer_id'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit missing data'),
      '#attributes' => [
        'class' => [
          'btn-primary',
          'ms-0',
          'd-block',
          'w-100',
        ],
      ],
      '#ajax' => [
        'callback' => '::submit',
        'wrapper' => 'apex-edge-cancel-service-form',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Submits the missing data form via ajax.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $service_request_id = $form_state->getValue('service_request_id');
    $customer_id = $form_state->getValue('customer_id');
    if ($service_request_id && $customer_id) {
      $missing_data = $form_state->getStorage()['missing_data'];
      $missing_data_answers = [];

      foreach ($missing_data as $item) {
        $missing_data_answers[] = new MissingDataParam(
          question: $item->question,
          answer: $form_state->getValue($item->field)
        );
      }

      $this->serviceRequestService->submitMissingInformation($customer_id, $service_request_id, $missing_data_answers);
      $this->loggerFactory->get('apexedge_forms')
        ->notice('Missing data for the Service request (id = ' . $service_request_id . ') submitted.');

      $response = new AjaxResponse();
      $response->addCommand(
        new ReplaceCommand(
          '#apex-edge-missing-data-form',
          Markup::create($form_state->getValue('success_message'))->__toString()
        )
      );
      return $response;
    }
  }

}
