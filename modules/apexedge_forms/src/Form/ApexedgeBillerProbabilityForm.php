<?php

namespace Drupal\apexedge_forms\Form;

use Drupal\apexedge\Services\BillerService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Biller Probability form.
 */
class ApexedgeBillerProbabilityForm extends FormBase {

  /**
   * Biller service.
   *
   * @var \Drupal\apexedge\Services\BillerService
   */
  protected $billerService;

  /**
   * The Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal token service container.
   *
   * @var Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * CancelBillForm constructor.
   */
  public function __construct(
    BillerService $biller_service,
    LoggerChannelFactoryInterface $factory,
    MessengerInterface $messenger,
    Token $token
  ) {
    $this->billerService = $biller_service;
    $this->loggerFactory = $factory;
    $this->messenger = $messenger;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('apexedge.biller_service'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('token'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apexedge_biller_probability';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $configs = []) {
    $form['#prefix'] = '<div id="apex-edge-biller-probability-form">';
    $form['#suffix'] = '</div>';

    $form['negotiation_available_message'] = [
      '#type' => 'hidden',
      '#value' => $configs['negotiation_available_message'] ?? '',
    ];

    $form['negotiation_unavailable_message'] = [
      '#type' => 'hidden',
      '#value' => $configs['negotiation_unavailable_message'] ?? '',
    ];

    $form['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $configs['title'] ?? '',
      '#attributes' => [
        'class' => [
          'body',
          'loga-palette-1',
          'text-center',
        ],
      ],
    ];

    $form['body'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $configs['body'] ?? '',
      '#attributes' => [
        'class' => [
          'body',
          'loga-palette-1',
        ],
      ],
    ];

    if ($configs['show_category']) {
      $form['categories'] = [
        '#type' => 'select',
        '#title' => $this->t('Category'),
        "#empty_option" => $this->t('- Select category -'),
        '#options' => [
          'Wireless' => $this->t('Wireless'),
          'Internet' => $this->t('Internet'),
          'Pay TV' => $this->t('Pay TV'),
          'Landline Phone' => $this->t('Landline Phone'),
          'Home Security' => $this->t('Home Security'),
          'Satellite Radio' => $this->t('Satellite Radio'),
          'Subscription Service' => $this->t('Subscription Service'),
          'Other' => $this->t('Other'),
        ],
        '#ajax' => [
          'callback' => '::getBillersForCategory',
          'event' => 'change',
          'wrapper' => 'billers-wrapper',
        ],
      ];
    }

    $form['biller_id'] = [
      '#type' => 'apexedge_billers',
      '#title' => $this->t('Service Provider'),
      "#empty_option" => $this->t('- Select service provider -'),
      '#required' => TRUE,
      '#prefix' => '<div id="billers-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['monthly_amount'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#title' => $this->t('Average monthly payment'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $configs['submit_text'],
      '#attributes' => [
        'class' => [
          'btn-primary',
          'ms-0',
          'd-block',
          'w-100',
        ],
      ],
      '#ajax' => [
        'callback' => '::submit',
        'wrapper' => 'apex-edge-biller-probability-form',
      ],
    ];

    return $form;
  }

  /**
   * Get billers for category.
   */
  public function getBillersForCategory(array $form, FormStateInterface $form_state) {
    $category = $form_state->getValue('categories');
    if ($category) {
      $form['biller_id']['#options'] = array_merge(['' => $this->t('- Select service provider -')], $this->billerService->getListAllBillersNamesByCategory($category));
    }
    else {
      $form['biller_id']['#options'] = array_merge(['' => $this->t('- Select service provider -')], $this->billerService->getlistAllBillersNames());
    }

    return $form['biller_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Submits the request via ajax and returns an answer to the user.
   */
  public function submit(array &$form, FormStateInterface $form_state) {
    $biller_id = $form_state->getValue('biller_id');
    $monthly_amount = $form_state->getValue('monthly_amount');
    $biller_probability = $this->billerService->getBillerProbability($biller_id, $monthly_amount);
    $confirmation_message = $this->token
      ->replace(
        $form_state->getValue('negotiation_available_message'),
        ['response' => $biller_probability]
      );
    $response = new AjaxResponse();
    $response->addCommand(
      new ReplaceCommand(
        '#apex-edge-biller-probability-form',
        Markup::create($confirmation_message)->__toString()
      )
    );
    return $response;
  }

}
