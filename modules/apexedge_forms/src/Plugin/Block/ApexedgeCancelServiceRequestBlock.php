<?php

namespace Drupal\apexedge_forms\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Apexedge - Cancel Service Request' block.
 *
 * @Block(
 *  id = "apexedge_cancel_service_request_block",
 *  admin_label = @Translation("Apexedge - Cancel Service Request"),
 *  category = @Translation("Apexedge")
 * )
 */
class ApexedgeCancelServiceRequestBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new RedirectFormBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return $this->formBuilder->getForm('Drupal\apexedge_forms\Form\ApexedgeCancelServiceRequestForm', $config);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['customer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer Id'),
      '#default_value' => $config['customer_id'] ?? '',
    ];

    $form['service_request_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Request ID'),
      '#default_value' => $config['service_request_id'] ?? NULL,
    ];

    $form['success_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Success Message'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['success_message'] ?? '',
    ];

    $form['cancel_service_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cancel service button Text'),
      '#required' => TRUE,
      '#default_value' => $config['cancel_service_button_text'] ?? 'Cancel service request',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['customer_id'] = $values['customer_id'];
    $this->configuration['service_request_id'] = $values['service_request_id'];
    $this->configuration['success_message'] = $values['success_message']['value'];
    $this->configuration['cancel_service_button_text'] = $values['cancel_service_button_text'];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIf(
      $account->hasPermission('cancel apexedge service request')
        || in_array('administrator', $account->getRoles())
        || $account->id() == '1');
  }

}
