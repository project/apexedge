<?php

namespace Drupal\apexedge_forms\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Apexedge - Service Request' block.
 *
 * @Block(
 *  id = "apexedge_service_request_block",
 *  admin_label = @Translation("Apexedge - Service Request"),
 *  category = @Translation("Apexedge")
 * )
 */
class ApexedgeServiceRequestBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new RedirectFormBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    return $this->formBuilder->getForm('Drupal\apexedge_forms\Form\ApexedgeServiceRequestForm', $config);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#default_value' => $config['title'] ?? '',
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['body'] ?? '',
    ];

    $form['confirmation_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Confirmation message'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['confirmation_message'] ?? '',
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Product type'),
      '#options' => [
        'negotiation' => $this->t('Negotiation'),
        'cancellation' => $this->t('Cancellation'),
        'enhancement' => $this->t('Enhancement'),
      ],
      '#required' => TRUE,
      '#default_value' => $config['type'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['title'] = $values['title'] ?? '';
    $this->configuration['body'] = $values['body']['value'] ?? '';
    $this->configuration['confirmation_message'] = $values['confirmation_message']['value'] ?? '';
    $this->configuration['type'] = $values['type'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIf(
      $account->hasPermission('create apexedge service request')
        || in_array('administrator', $account->getRoles())
        || $account->id() == '1');
  }

}
