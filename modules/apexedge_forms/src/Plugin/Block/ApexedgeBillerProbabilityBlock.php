<?php

namespace Drupal\apexedge_forms\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Apexedge - Biller Probability' block.
 *
 * @Block(
 *  id = "apexedge_biller_probability_block",
 *  admin_label = @Translation("Apexedge - Biller Probability"),
 *  category = @Translation("Apexedge")
 * )
 */
class ApexedgeBillerProbabilityBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new RedirectFormBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return $this->formBuilder->getForm('Drupal\apexedge_forms\Form\ApexedgeBillerProbabilityForm', $config);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#default_value' => $config['title'] ?? '',
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['body'] ?? '',
    ];

    $form['show_category'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Category Selection'),
      '#default_value' => $config['show_category'] ?? '',
    ];

    $form['submit_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit Text'),
      '#default_value' => $config['submit_text'] ?? '',
    ];

    $form['negotiation_available_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Negotiation Available Message'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['negotiation_available_message'] ?? '',
    ];

    $form['negotiation_unavailable_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Negotiation Unavailable Message'),
      '#format' => 'full_html',
      '#required' => TRUE,
      '#default_value' => $config['negotiation_unavailable_message'] ?? '',
    ];

    $form['negotiation_threshold_property'] = [
      '#type' => 'radios',
      '#title' => $this->t('Negotiation Threshold Property'),
      '#options' => [
        'savings_rate' => $this->t('Monthly Savings Rate'),
        'success_rate' => $this->t('Success Rate'),
      ],
      '#required' => TRUE,
      '#default_value' => $config['negotiation_threshold_property'] ?? 'savings_rate',
    ];

    $form['negotiation_threshold_value'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#title' => $this->t('Negotiation Threshold Value'),
      '#required' => TRUE,
      '#default_value' => $config['negotiation_threshold_value'] ?? '',
      '#description' => Markup::create('<ul>
<li>What potential saving$ (Savings Rate) amount is it meaningful enough to trigger a CTA? We suggest ~$5 month/$60 annually minimum.</li>
<li>What average Success Rate is sufficient to present a CTA? We suggest 45%+.</li>
</ul>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['title'] = $values['title'];
    $this->configuration['body'] = $values['body']['value'];
    $this->configuration['show_category'] = $values['show_category'];
    $this->configuration['submit_text'] = $values['submit_text'];
    $this->configuration['negotiation_threshold_property'] = $values['negotiation_threshold_property'];
    $this->configuration['negotiation_threshold_value'] = $values['negotiation_threshold_value'];
    $this->configuration['negotiation_available_message'] = $values['negotiation_available_message']['value'];
    $this->configuration['negotiation_unavailable_message'] = $values['negotiation_unavailable_message']['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIf(
      $account->hasPermission('create apexedge service request')
        || in_array('administrator', $account->getRoles())
        || $account->id() == '1');
  }

}
