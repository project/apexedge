<?php

namespace Drupal\apexedge_notifications\EventSubscriber;

use Drupal\apexedge\Events\ServiceRequestCreatedWebhookEvent;
use Drupal\apexedge\Events\ServiceRequestStageUpdatedWebhookEvent;
use Drupal\apexedge_notifications\Services\ApexEdgeNotificationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts on Service Request Stage Updated events.
 */
class ServiceRequestStageUpdatedEventSubscriber implements EventSubscriberInterface {

  /**
   * Notifications service.
   *
   * @var \Drupal\apexedge_notifications\Services\ApexEdgeNotificationService
   */
  protected ApexEdgeNotificationService $notificationsService;

  /**
   * Constructs a new BillerEventsSubscriber object.
   */
  public function __construct(ApexEdgeNotificationService $notifications_service) {
    $this->notificationsService = $notifications_service;
  }

  /**
   * Sends service request state update notification.
   */
  public function sendServiceRequestStateUpdatedNotification(ServiceRequestStageUpdatedWebhookEvent $event) {
    $this->notificationsService->sendServiceRequestStateUpdatedNotification($event);
  }

  /**
   * Sends service request create notification.
   */
  public function sendServiceRequestCreateNotification(ServiceRequestCreatedWebhookEvent $event) {
    $this->notificationsService->sendServiceRequestCreateNotification($event);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ServiceRequestStageUpdatedWebhookEvent::SERVICE_REQUEST_STAGE_UPDATED_WEBHOOK][] = ['sendServiceRequestStateUpdatedNotification'];
    $events[ServiceRequestCreatedWebhookEvent::SERVICE_REQUEST_CREATED_WEBHOOK][] = ['sendServiceRequestCreateNotification'];
    return $events;
  }

}
