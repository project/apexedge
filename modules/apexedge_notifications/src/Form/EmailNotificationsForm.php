<?php

namespace Drupal\apexedge_notifications\Form;

use Drupal\apexedge_notifications\EmailNotificationsConstants;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for notification emails.
 */
class EmailNotificationsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'apexedge_notifications.email_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'apexedge_notifications_email_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::SETTINGS);

    $form['#attached']['library'][] = 'token/jquery.treeTable';

    $form['text']['#markup'] = $this->t('
      <p>Before enabling any notification ensure you have the right communication settings with ApexEdge.</p>
      <p>Check the <a href="https://www.apexedge.com/partner-portal#Communications_Copy" target="_blank">Communications Copy</a> ApexEdge provides in order to help you write the best copy for your notifications.</p>
      <p>Check the available tokens for each of the notification emails.</p>
    ');

    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#global_types' => TRUE,
      '#show_nested' => FALSE,
    ];

    $form['notification_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings'),
    ];

    $form['notifications'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Service Request Status Update Notifications'),
    ];

    foreach (EmailNotificationsConstants::notificationsList() as $notification => $title) {
      $form[$notification . '_details'] = [
        '#type' => 'details',
        '#title' => $title,
        '#open' => FALSE,
        '#group' => 'notifications',
      ];

      $form[$notification . '_details'][$notification . '_enable'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $config->get($notification . '_enable') ?? '0',
      ];

      $subject_config = $notification . '_subject';
      $form[$notification . '_details'][$subject_config] = [
        '#type' => 'textfield',
        '#title' => $this->t('Please add the email subject for @notification mail', ['@notification' => $title]),
        '#default_value' => $config->get($subject_config) ?? '',
      ];

      $form[$notification . '_details'][$notification] = [
        '#type' => 'text_format',
        '#title' => $this->t('Please add the email body for @notification mail', ['@notification' => $title]),
        '#format' => 'full_html',
        '#default_value' => $config->get($notification) ?? '',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS);
    foreach (EmailNotificationsConstants::notificationsList() as $notification => $title) {
      $subject_config = $notification . '_subject';
      $config->set($notification, $form_state->getValue($notification)['value'])
        ->set($subject_config, $form_state->getValue($subject_config))
        ->set($notification . '_enable', $form_state->getValue($notification . '_enable'));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
