<?php

namespace Drupal\apexedge_notifications\Services;

use Apexedge\DTO\ServiceRequest\ServiceRequest;
use Apexedge\DTO\ServiceRequest\Status\ServiceRequestStage;
use Drupal\apexedge\Events\ServiceRequestCreatedWebhookEvent;
use Drupal\apexedge\Events\ServiceRequestStageUpdatedWebhookEvent;
use Drupal\apexedge_notifications\EmailNotificationsConstants;
use Drupal\apexedge_notifications\Form\EmailNotificationsForm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;

/**
 * ApexEdge Notification Service.
 */
class ApexEdgeNotificationService {

  use StringTranslationTrait;

  /**
   * Constructor for ApexEdgeNotification Email service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactoryInterface $logger,
    protected MailManagerInterface $mailManager,
    protected RendererInterface $renderer,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Token $token,
    protected Connection $connection
  ) {
  }

  /**
   * Sends service request state updated notification.
   */
  public function sendServiceRequestStateUpdatedNotification(ServiceRequestStageUpdatedWebhookEvent $event): bool {
    $stage = $event->payload->service_request->stage->getStage();
    $customer_id = $event->payload->customer_id;
    $service_request = $event->payload->service_request;
    return $this->sendNotification($customer_id, $stage, $service_request);
  }

  /**
   * Sends service request create notification.
   */
  public function sendServiceRequestCreateNotification(ServiceRequestCreatedWebhookEvent $event): bool {
    $stage = $event->payload->service_request->stage->getStage();
    $customer_id = $event->payload->customer_id;
    $service_request = $event->payload->service_request;
    return $this->sendNotification($customer_id, $stage, $service_request);
  }

  /**
   * Sends notification.
   */
  public function sendNotification(string $customer_id, ServiceRequestStage $stage, ServiceRequest $service_request): bool {
    $notification_id = $this->getNotificationIdByStage($stage);
    $user_id = $this->getUserId($customer_id);
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($user_id);
    $params['service_request'] = $service_request;
    if (!$this->shouldSendOptionalNotification($notification_id) || !$user) {
      return FALSE;
    }
    return $this->sendEmail($notification_id, $user->getEmail(), 'en', $params);
  }

  /**
   * Gets notification id by stage.
   */
  public function getNotificationIdByStage(ServiceRequestStage $stage): string {
    return match ($stage) {
      ServiceRequestStage::NEW => EmailNotificationsConstants::NEW,
      ServiceRequestStage::PENDING => EmailNotificationsConstants::PENDING,
      ServiceRequestStage::ASSIGNED => EmailNotificationsConstants::ASSIGNED,
      ServiceRequestStage::IN_PROGRESS => EmailNotificationsConstants::IN_PROGRESS,
      ServiceRequestStage::MISSING_DATA => EmailNotificationsConstants::MISSING_DATA,
      ServiceRequestStage::ON_HOLD => EmailNotificationsConstants::ON_HOLD,
      ServiceRequestStage::DUPLICATE => EmailNotificationsConstants::DUPLICATE,
      ServiceRequestStage::REQUEST_CANCELED => EmailNotificationsConstants::REQUEST_CANCELED,
      ServiceRequestStage::INVALID_REQUEST => EmailNotificationsConstants::INVALID_REQUEST,
      ServiceRequestStage::COMPLETE_EXISTING_PROMOS => EmailNotificationsConstants::COMPLETE_EXISTING_PROMOS,
      ServiceRequestStage::COMPLETED_NO_SAVINGS => EmailNotificationsConstants::COMPLETED_NO_SAVINGS,
      ServiceRequestStage::COMPLETED_SAVINGS_ACHIEVED => EmailNotificationsConstants::COMPLETED_SAVINGS_ACHIEVED,
      default => throw new \InvalidArgumentException("No constant for the status: $stage->value")
    };
  }

  /**
   * Gets the user id.
   */
  public function getUserId(string $customer_id) {
    $query = $this->connection->select('users_data', 'ursd');
    $query->addField('ursd', 'uid');
    $query->condition('module', 'apexedge');
    $query->condition('name', 'customer_id');
    $query->condition('value', $customer_id);
    return $query->execute()->fetchField();
  }

  /**
   * Populates the email.
   *
   * @throws \Exception
   */
  public function populateEmail($key, &$message, $params): void {
    $available_notifications = array_keys(EmailNotificationsConstants::notificationsList());

    if (!in_array($key, $available_notifications)) {
      return;
    }

    $notification_message = $this->getEmailBodyFor($key);
    $subject = $this->getEmailSubjectFor($key);
    if (empty($notification_message) || empty($subject)) {
      return;
    }

    $service_request = $params['service_request'] ?? '';
    if ($service_request instanceof ServiceRequest) {
      $notification_message = $this->replaceServiceRequestTokens($notification_message, $service_request);
      $subject = $this->replaceServiceRequestTokens($subject, $service_request);
    }

    // Configurations for email.
    $message['subject'] = $subject;
    $message['headers']['Content-Type'] = 'text/html';
    $message['body'][] = $notification_message;
    $message['from'] = $this->configFactory->get('system.site')->get('mail');
  }

  /**
   * Gets the email subject for a specific notification.
   */
  private function getEmailSubjectFor(string $mail_id) {
    return $this->configFactory->get(EmailNotificationsForm::SETTINGS)
      ->get($mail_id . '_subject');
  }

  /**
   * Get the email body for a specific notification.
   */
  private function getEmailBodyFor(string $mail_id) {
    return $this->configFactory->get(EmailNotificationsForm::SETTINGS)
      ->get($mail_id);
  }

  /**
   * Wrapper to mailManager->mail that logs error if email was not sent.
   */
  private function sendEmail($key, $to, $langcode = 'en', $params = [], $reply = NULL, $send = TRUE): bool {
    $result = $this->mailManager->mail(
      'apexedge_notifications',
      $key,
      $to,
      $langcode,
      $params,
      $reply,
      $send
    );

    if ($result['result'] !== TRUE) {
      $this->logger->get('apexedge_notifications')
        ->error($this->t('There was a problem sending your message and it was not sent.'));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Replace tokens on a string.
   */
  private function replaceServiceRequestTokens($string, ServiceRequest $service_request): string {
    $token_data = [
      'service_request' => $service_request,
    ];
    $token_options = ['clear' => TRUE];
    return $this->token->replace($string, $token_data, $token_options);
  }

  /**
   * Checks if an optional notification is enabled to be sent.
   */
  private function shouldSendOptionalNotification(string $notification): bool {
    $optional_notifications = $this->configFactory
      ->get(EmailNotificationsForm::SETTINGS)
      ->get($notification . '_enable');

    return (bool) $optional_notifications;
  }

}
