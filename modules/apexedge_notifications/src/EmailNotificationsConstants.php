<?php

namespace Drupal\apexedge_notifications;

/**
 * Constants of all notifications of ApexEdge module.
 */
class EmailNotificationsConstants {

  /**
   * New.
   */
  const NEW = 'new_service_request';

  /**
   * Pending.
   */
  const PENDING = 'pending_service_request';

  /**
   * Assigned.
   */
  const ASSIGNED = 'assigned_service_request';

  /**
   * In progress.
   */
  const IN_PROGRESS = 'in_progress_service_request';

  /**
   * Missing data.
   */
  const MISSING_DATA = 'missing_data_service_request';

  /**
   * On hold.
   */
  const ON_HOLD = 'on_hold_service_request';

  /**
   * Duplicate.
   */
  const DUPLICATE = 'duplicate_service_request';

  /**
   * Request canceled.
   */
  const REQUEST_CANCELED = 'request_canceled_service_request';

  /**
   * Invalid request.
   */
  const INVALID_REQUEST = 'invalid_request_service_request';

  /**
   * Complete existing promos.
   */
  const COMPLETE_EXISTING_PROMOS = 'complete_existing_promos_service_request';

  /**
   * Completed no savings.
   */
  const COMPLETED_NO_SAVINGS = 'completed_no_savings_service_request';

  /**
   * Completed savings achieved.
   */
  const COMPLETED_SAVINGS_ACHIEVED = 'completed_savings_achieved_service_request';

  /**
   * List of all notifications.
   *
   * @return string[]
   *   The list of notifications available.
   */
  public static function notificationsList(): array {
    return [
      self::NEW => 'New',
      self::ASSIGNED => 'Assigned',
      self::PENDING => 'Pending',
      self::IN_PROGRESS => 'In progress',
      self::MISSING_DATA => 'Missing data',
      self::ON_HOLD => 'On hold',
      self::DUPLICATE => 'Duplicate',
      self::REQUEST_CANCELED => 'Request canceled',
      self::INVALID_REQUEST => 'Invalid request',
      self::COMPLETE_EXISTING_PROMOS => 'Complete existing promos',
      self::COMPLETED_NO_SAVINGS => 'Completed no savings',
      self::COMPLETED_SAVINGS_ACHIEVED => 'Completed savings achieved',
    ];
  }

}
