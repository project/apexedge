<?php

namespace Drupal\apexedge\Services;

use Apexedge\DTO\ServiceRequest\CustomerAccountParams;
use Apexedge\DTO\ServiceRequest\ServiceRequestParams;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\apexedge\Events\ApexEdgeApiErrorEvent;
use Drupal\apexedge\Events\CustomerAccountCreatedEvent;
use Drupal\apexedge\Events\ServiceRequestCancelledEvent;
use Drupal\apexedge\Events\ServiceRequestCreatedEvent;
use Drupal\apexedge\Events\SubmitMissingInformationEvent;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserDataInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * ApexEdge Service Request service.
 */
class ServiceRequestService {

  /**
   * Drupal Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Messenger Interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ApexEdge service.
   *
   * @var \Drupal\apexedge\Services\ApexEdgeService
   */
  protected $apexEdgeService;

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Constructor.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_channel_factory,
    MessengerInterface $messenger,
    ApexEdgeService $apex_edge_service,
    EventDispatcherInterface $event_dispatcher,
    AccountInterface $current_user,
    UserDataInterface $user_data
  ) {
    $this->loggerChannelFactory = $logger_channel_factory;
    $this->messenger = $messenger;
    $this->apexEdgeService = $apex_edge_service;
    $this->eventDispatcher = $event_dispatcher;
    $this->currentUser = $current_user;
    $this->userData = $user_data;
  }

  /**
   * Create Customer Account.
   *
   * @param \Apexedge\DTO\ServiceRequest\CustomerAccountParams $account_params
   *   Account params object.
   */
  public function createCustomerAccount(CustomerAccountParams $account_params) {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $customer_account = $apexEdge->createCustomerAccount($account_params);
      $this->userData->set('apexedge', $this->currentUser->id(), 'customer_id', $customer_account->id);
      $this->eventDispatcher->dispatch(new CustomerAccountCreatedEvent($customer_account), CustomerAccountCreatedEvent::CUSTOMER_ACCOUNT_CREATED);
      return $customer_account->id;
    }
    catch (InvalidApexedgeRequest $e) {
      if ($e->getResponse()->getErrorCode() == 'record_exists') {
        $body = json_decode($e->getResponse()->getRaw()->getBody(), TRUE);
        $customerId = $body['customer']['id'];
        $this->userData->set('apexedge', $this->currentUser->id(), 'customer_id', $customerId);
        return $customerId;
      }
      $this->eventDispatcher->dispatch(new ApexEdgeApiErrorEvent($e), ApexEdgeApiErrorEvent::APEXEDGE_API_ERROR);
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

  /**
   * Create Service.
   *
   * @param string $customer_id
   *   value of customer ID.
   * @param \Apexedge\DTO\ServiceRequest\ServiceRequestParams $service_request_params
   *   Service request params object.
   */
  public function createServiceRequest(string $customer_id, ServiceRequestParams $service_request_params) {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $service_request = $apexEdge->createServiceRequest($customer_id, $service_request_params);
      $this->eventDispatcher->dispatch(new ServiceRequestCreatedEvent($service_request), ServiceRequestCreatedEvent::SERVICE_REQUEST_CREATED);
      return $service_request;
    }
    catch (InvalidApexedgeRequest $e) {
      $this->eventDispatcher->dispatch(new ApexEdgeApiErrorEvent($e), ApexEdgeApiErrorEvent::APEXEDGE_API_ERROR);
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

  /**
   * Cancel Service.
   *
   * @param string $customer_id
   *   value of customer ID.
   * @param string $service_request_id
   *   Service request ID.
   */
  public function cancelServiceRequest(string $customer_id, string $service_request_id) {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $apexEdge->cancelServiceRequest($customer_id, $service_request_id);
      $this->eventDispatcher->dispatch(new ServiceRequestCancelledEvent($service_request_id), ServiceRequestCancelledEvent::SERVICE_REQUEST_CANCELLED);
    }
    catch (InvalidApexedgeRequest $e) {
      $this->eventDispatcher->dispatch(new ApexEdgeApiErrorEvent($e), ApexEdgeApiErrorEvent::APEXEDGE_API_ERROR);
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

  /**
   * Submit missing information.
   *
   * @param string $customer_id
   *   value of customer ID.
   * @param string $service_request_id
   *   Service request ID.
   * @param \Apexedge\DTO\ServiceRequest\MissingDataParam[] $missing_data
   *   Missing data.
   */
  public function submitMissingInformation(string $customer_id, string $service_request_id, array $missing_data) {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $response = $apexEdge->submitMissingInformation($customer_id, $service_request_id, $missing_data);
      $this->eventDispatcher->dispatch(new SubmitMissingInformationEvent($response), SubmitMissingInformationEvent::SUBMIT_MISSING_INFORMATION);
    }
    catch (InvalidApexedgeRequest $e) {
      $this->eventDispatcher->dispatch(new ApexEdgeApiErrorEvent($e), ApexEdgeApiErrorEvent::APEXEDGE_API_ERROR);
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

}
