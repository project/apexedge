<?php

namespace Drupal\apexedge\Services;

use Apexedge\DTO\Biller\Biller;
use Apexedge\DTO\Biller\BillerProbability;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\apexedge\Events\ApexEdgeApiErrorEvent;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\State;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * ApexEdge service.
 */
class BillerService {

  public const  LIST_ALL_BILLERS_PROBABILITIES = 'apexedge.list_all_billers_probabilities';
  public const  LAST_APEXEDGE_BILLERS_SYNC = 'apexedge.last_apexedge_billers_sync';

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Drupal Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Messenger Interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ApexEdge service.
   *
   * @var \Drupal\apexedge\Services\ApexEdgeService
   */
  protected $apexEdgeService;

  /**
   * Drupal state.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_channel_factory,
    MessengerInterface $messenger,
    ApexEdgeService $apex_edge_service,
    State $state,
    TimeInterface $time,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $logger_channel_factory;
    $this->messenger = $messenger;
    $this->apexEdgeService = $apex_edge_service;
    $this->state = $state;
    $this->time = $time;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Sync bilers.
   */
  public function syncBillers() {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $list_all_billers_probabilities = $apexEdge->listAllBillersProbabilities();
      $this->state->set(self::LIST_ALL_BILLERS_PROBABILITIES, $list_all_billers_probabilities);
      $this->state->set(self::LAST_APEXEDGE_BILLERS_SYNC, $this->time->getCurrentTime());
    }
    catch (InvalidApexedgeRequest $e) {
      $this->eventDispatcher->dispatch(new ApexEdgeApiErrorEvent($e), ApexEdgeApiErrorEvent::APEXEDGE_API_ERROR);
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

  /**
   * Get biller probability.
   *
   * @return \Apexedge\DTO\Biller\BillerProbability
   *   Biller probabilities.
   */
  public function getBillerProbability(string $biller_id, float $monthly_amount): BillerProbability {
    $biller = $this->getBiller($biller_id);
    return $biller->getProbabilityForMonthlyExpense($monthly_amount);
  }

  /**
   * Get sync time.
   *
   * @return mixed|null
   *   Time of sync.
   */
  public function getLastApexEdgeBillersSyncTime() {
    return $this->state->get(self::LAST_APEXEDGE_BILLERS_SYNC);
  }

  /**
   * Get list all Billers names.
   *
   * @return array
   *   List of billers names.
   */
  public function getlistAllBillersNames(): array {
    $billers_names = [];
    $list_all_billers = $this->getAllBillers();
    foreach ($list_all_billers as $key => $value) {
      $billers_names[$key] = $value->name;
    }
    return $billers_names;
  }

  /**
   * Get Billers.
   *
   * @return \Apexedge\DTO\Biller\Biller[]
   *   Billers.
   */
  public function getAllBillers(): array {
    $billers = $this->state->get(self::LIST_ALL_BILLERS_PROBABILITIES);
    if (empty($billers)) {
      $this->syncBillers();
      $billers = $this->state->get(self::LIST_ALL_BILLERS_PROBABILITIES);
    }
    return $billers ?? [];
  }

  /**
   * Get Biller.
   *
   * @param string $biller_id
   *   Biller ID.
   *
   * @return \Apexedge\DTO\Biller\Biller
   *   Biller.
   */
  public function getBiller(string $biller_id): Biller {
    $billers = $this->getAllBillers();
    return $billers[$biller_id] ?? throw new InvalidApexedgeRequest('The biller with id ' . $biller_id . ' does not exist.');
  }

}
