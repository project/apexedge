<?php

namespace Drupal\apexedge\Services;

use Apexedge\ApexedgeClient;
use Apexedge\ApexedgeResponse;
use Drupal\apexedge\Form\ApexEdgeSettingsForm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\key\KeyRepository;
use GuzzleHttp\Client;

/**
 * ApexEdge service.
 */
class ApexEdgeService {


  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Drupal Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Drupal Key module repository.
   *
   * @var \Drupal\key\KeyRepository
   */
  private $keyRepository;

  /**
   * Messenger Interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Apexedge client.
   *
   * @var \Apexedge\ApexedgeClient
   */
  protected $client;

  /**
   * Constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_channel_factory,
    KeyRepository $key_repository,
    MessengerInterface $messenger
  ) {
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $logger_channel_factory;
    $this->keyRepository = $key_repository;
    $this->messenger = $messenger;
    $this->initClient();
  }

  /**
   * Log API error.
   */
  public function logApiError(ApexedgeResponse $response) {
    $this->messenger->addError($response->getErrorMessage());
    $this->loggerChannelFactory
      ->get('apexedge')
      ->error("
          Unable to perform Apexedge API request:" . $response->getCMD() . "<br/>
          <strong>Error message: </strong>" . $response->getErrorMessage() . "<br/>
          <strong>Error code: </strong>" . $response->getErrorCode() . "<br/>
        ");
  }

  /**
   * Get ApexedgeClient.
   *
   * @return \Apexedge\ApexedgeClient
   *   The ApexedgeClient client.
   */
  public function getClient(): ApexedgeClient {
    return $this->client;
  }

  /**
   * Get ApexedgeClient.
   *
   * @throws \Exception
   */
  private function initClient() {
    $client = new Client();
    $config = $this->configFactory->get(ApexEdgeSettingsForm::SETTINGS);
    $key_id = $config->get('api_key');
    $key = $key_id ? $this->keyRepository->getKey($key_id) : NULL;
    $api_key = $key ? $key->getKeyValue() : '';
    $this->client = new ApexedgeClient($client, $api_key);
  }

}
