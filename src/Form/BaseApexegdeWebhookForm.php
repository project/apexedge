<?php

namespace Drupal\apexedge\Form;

use Apexedge\DTO\Webhook\Events\EnabledEvents;
use Apexedge\DTO\Webhook\Webhook;
use Apexedge\DTO\Webhook\WebhookParams;
use Drupal\apexedge\Services\ApexEdgeService;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for all Apexegde Webhook form.
 */
abstract class BaseApexegdeWebhookForm extends FormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The access manager service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * ApexEdge service.
   *
   * @var \Drupal\apexedge\Services\ApexEdgeService
   */
  protected $apexEdgeService;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   * @param \Drupal\apexedge\Services\ApexEdgeService $apex_edge_service
   *   ApexEdge service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    CurrentRouteMatch $route_match,
    ApexEdgeService $apex_edge_service
  ) {
    $this->currentUser = $current_user;
    $this->routeMatch = $route_match;
    $this->apexEdgeService = $apex_edge_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('apexedge.apexedge_service'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webhook $webhook = NULL) {

    $events = EnabledEvents::cases();
    $enabled_events = [];
    foreach ($events as $event) {
      $enabled_events[$event->value] = $event->name;
    }

    if ($webhook) {
      $form['id'] = [
        '#title' => $this->t('ID'),
        '#type' => 'textfield',
        '#attributes' => [
          'readonly' => 'readonly',
        ],
        '#default_value' => $webhook->id ?? '',
      ];
    }

    $form['url'] = [
      '#title' => $this->t('URL'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => $this->t('The url of the webhook. Should be an absolute path.'),
      '#default_value' => $webhook->url ?? Url::fromRoute('apexedge.webhook_listener', [], ['absolute' => TRUE])->toString(),
    ];

    $form['enabled_events'] = [
      '#title' => $this->t('Enabled events'),
      '#type' => 'checkboxes',
      '#options' => $enabled_events,
      '#required' => TRUE,
      '#default_value' => $webhook->enabled_events ?? [],
    ];

    $form['payload_included'] = [
      '#title' => $this->t('Payload included'),
      '#type' => 'checkbox',
      '#default_value' => $webhook->payload_included ?? '1',
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => $this->t('Save'),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
        ],
      ],
    ];

    if ($webhook) {
      $form['delete'] = [
        '#type' => 'submit',
        '#name' => 'delete',
        '#value' => $this->t('Delete'),
        '#attributes' => [
          'class' => [
            'action-link',
            'action-link--danger',
            'action-link--icon-trash',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookParamsFromForm(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $enabled_events = [];
    foreach ($values['enabled_events'] ?? [] as $key => $enabled_event) {
      if ($enabled_event != 0) {
        $enabled_events[] = EnabledEvents::from($key);
      }
    }

    return new WebhookParams(
      enabled_events: $enabled_events,
      url: $values['url'],
      payload_included: $values['payload_included'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url = $form_state->getValue('url');
    if (!UrlHelper::isValid($url, TRUE)) {
      $form_state->setErrorByName('url', $this->t('The url is not valid. An absolute url has to be provided.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  abstract public function submitForm(array &$form, FormStateInterface $form_state);

}
