<?php

namespace Drupal\apexedge\Form;

use Apexedge\DTO\Webhook\Webhook;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\Core\Form\FormStateInterface;

/**
 * Apexegde delete webhook form.
 */
class DeleteApexegdeWebhookForm extends BaseApexegdeWebhookForm {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'apexegde_delete_webhook_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webhook $webhook = NULL) {

    $form['confirm_text'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('This action cannot be undone.'),
    ];

    $form['delete'] = [
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => $this->t('Delete'),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
        ],
      ],
    ];

    $form['cancel'] = [
      '#type' => 'submit',
      '#name' => 'cancel',
      '#value' => $this->t('Cancel'),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $webhook_id = $this->routeMatch->getParameter('wid');
    $apexEdge = $this->apexEdgeService->getClient();
    if ($form_state->getTriggeringElement()['#name'] == 'delete') {
      try {
        $apexEdge->deleteWebhook($webhook_id);
        $form_state->setRedirect('apexedge.all_webhook');
      }
      catch (InvalidApexedgeRequest $e) {
        $this->apexEdgeService->logApiError($e->getResponse());
      }
    }
    elseif ($form_state->getTriggeringElement()['#name'] == 'cancel') {
      $form_state->setRedirect('apexedge.edit_webhook', ['wid' => $webhook_id]);
    }
  }

}
