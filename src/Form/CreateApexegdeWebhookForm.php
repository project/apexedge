<?php

namespace Drupal\apexedge\Form;

use Apexedge\DTO\Webhook\Webhook;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\Core\Form\FormStateInterface;

/**
 * Apexegde create webhook form.
 */
class CreateApexegdeWebhookForm extends BaseApexegdeWebhookForm {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'apexegde_create_webhook_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webhook $webhook = NULL) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $apexEdge = $this->apexEdgeService->getClient();
    $webhook_params = $this->getWebhookParamsFromForm($form_state);

    try {
      $webhook = $apexEdge->createWebhook($webhook_params);
      if ($webhook->id) {
        $form_state->setRedirect('apexedge.edit_webhook', ['wid' => $webhook->id]);
      }
    }
    catch (InvalidApexedgeRequest $e) {
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

}
