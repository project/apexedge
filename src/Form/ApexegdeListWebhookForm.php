<?php

namespace Drupal\apexedge\Form;

use Apexedge\DTO\Webhook\Webhook;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Apexegde list webhooks form.
 */
class ApexegdeListWebhookForm extends BaseApexegdeWebhookForm {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'apexegde_list_webhooks_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webhook $webhook = NULL) {

    $form['add_webhook'] = [
      '#type' => 'submit',
      '#name' => 'add_webhook',
      '#value' => $this->t('Add webhook'),
      '#attributes' => [
        'class' => [
          'button',
          'button--action',
          'button--primary',
        ],
      ],
    ];

    // Create table header.
    $header = [
      'id' => $this->t('Id'),
      'enabled_events' => $this->t('Enabled events'),
      'url' => $this->t('URL'),
      'payload_included' => $this->t('Payload included'),
      'opt' => $this->t('Operations'),
    ];

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getWebhooks(),
      '#empty' => $this->t('No records found'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('apexedge.create_webhook');
  }

  /**
   * {@inheritdoc}
   */
  private function getWebhooks() {
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $webhooks = $apexEdge->listAllWebhooks();
    }
    catch (InvalidApexedgeRequest $e) {
      $this->apexEdgeService->logApiError($e->getResponse());
    }

    if (empty($webhooks)) {
      return [];
    }
    $rows = [];
    foreach ($webhooks as $webhook) {

      $link_options = [
        'attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];
      $url = Url::fromRoute('apexedge.edit_webhook', ['wid' => $webhook->id]);
      $url->setOptions($link_options);
      $edit_link = Link::fromTextAndUrl('Edit', $url)->toString();
      $rows[] = [
        'id' => $webhook->id,
        'enabled_events' => Markup::create(implode('<br>', $webhook->enabled_events ?? [])),
        'url' => $webhook->url,
        'payload_included' => $webhook->payload_included,
        'opt' => $edit_link,
      ];
    }

    return $rows;
  }

}
