<?php

namespace Drupal\apexedge\Form;

use Apexedge\DTO\Webhook\Webhook;
use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\Core\Form\FormStateInterface;

/**
 * Apexegde edit webhook form.
 */
class EditApexegdeWebhookForm extends BaseApexegdeWebhookForm {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'apexegde_edit_webhook_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webhook $webhook = NULL) {
    $webhook_id = $this->routeMatch->getParameter('wid');
    try {
      $apexEdge = $this->apexEdgeService->getClient();
      $apex_edge_webhook = $apexEdge->getWebhook($webhook_id);
      return parent::buildForm($form, $form_state, $apex_edge_webhook);
    }
    catch (InvalidApexedgeRequest $e) {
      $this->apexEdgeService->logApiError($e->getResponse());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $webhook_id = $this->routeMatch->getParameter('wid');
    if ($form_state->getTriggeringElement()['#name'] == 'delete') {
      $form_state->setRedirect('apexedge.delete_webhook', ['wid' => $webhook_id]);
    }
    elseif ($form_state->getTriggeringElement()['#name'] == 'save') {
      $apexEdge = $this->apexEdgeService->getClient();
      $webhook_params = $this->getWebhookParamsFromForm($form_state);

      try {
        $apexEdge->updateWebhook($webhook_id, $webhook_params);
      }
      catch (InvalidApexedgeRequest $e) {
        $this->apexEdgeService->logApiError($e->getResponse());
      }
    }
  }

}
