<?php

namespace Drupal\apexedge\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an ApexEdge Api settings form for ApexEdge Integration.
 */
class ApexEdgeSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'apexedge.apexedge_api_settings';

  /**
   * Biller service.
   *
   * @var \Drupal\apexedge\Services\BillerService
   */
  protected $billerService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->billerService = $container->get('apexedge.biller_service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "apexedge_api_settings_form";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::SETTINGS);

    $form['apexedge_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings'),
    ];

    $form['api_settings_details'] = [
      '#type' => 'details',
      '#title' => $this->t('API Settings'),
      '#open' => FALSE,
      '#group' => 'apexedge_settings',
    ];

    $form['api_settings_details']['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sandbox'),
      '#default_value' => $config->get('use_sandbox') ?? '',
    ];

    $form['api_settings_details']['api_key'] = [
      '#type' => 'key_select',
      '#key_filters' => [
        'type' => 'authentication',
        'provider' => 'config',
      ],
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => ($config->get('api_key')) ?: 'default_apexedge_api_key',
    ];

    $form['billers_settings_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Billers'),
      '#open' => FALSE,
      '#group' => 'apexedge_settings',
    ];

    $form['billers_settings_details']['biller_cache_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Biller Cache Duration'),
      '#field_suffix' => $this->t('days'),
      '#default_value' => 30,
    ];
    $last_apexedge_billers_sync = $this->billerService->getLastApexEdgeBillersSyncTime();
    $form['billers_settings_details']['last_apexedge_billers_sync'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Last ApexEdge Billers Sync'),
      '#attributes' => [
        'readonly' => 'readonly',
      ],
      '#default_value' => $last_apexedge_billers_sync ? DrupalDateTime::createFromTimestamp($last_apexedge_billers_sync) : '',
    ];

    $form['billers_settings_details']['sync_now'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync Now'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(self::SETTINGS)
      ->set('use_sandbox', $form_state->getValue('use_sandbox'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('biller_cache_duration', $form_state->getValue('biller_cache_duration'))
      ->save();

    if ($form_state->getValue('sync_now')) {
      $this->billerService->syncBillers();
    }

    parent::submitForm($form, $form_state);
  }

}
