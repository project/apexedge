<?php

namespace Drupal\apexedge\EventSubscriber;

use Drupal\apexedge\Events\BillerCreatedWebhookEvent;
use Drupal\apexedge\Events\BillerDeletedWebhookEvent;
use Drupal\apexedge\Events\BillerUpdatedWebhookEvent;
use Drupal\apexedge\Services\BillerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Reacts on Biller events.
 */
class BillerEventsSubscriber implements EventSubscriberInterface {

  /**
   * Biller service.
   *
   * @var \Drupal\apexedge\Services\BillerService
   */
  protected $billerService;

  /**
   * Constructs a new BillerEventsSubscriber object.
   */
  public function __construct(BillerService $biller_service) {
    $this->billerService = $biller_service;
  }

  /**
   * {@inheritdoc}
   */
  public function syncBillersData(RequestEvent $event) {
    $this->billerService->syncBillers();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[BillerCreatedWebhookEvent::BILLER_CREATED_WEBHOOK][] = ['syncBillersData'];
    $events[BillerDeletedWebhookEvent::BILLER_DELETED_WEBHOOK][] = ['syncBillersData'];
    $events[BillerUpdatedWebhookEvent::BILLER_UPDATED_WEBHOOK][] = ['syncBillersData'];
    return $events;
  }

}
