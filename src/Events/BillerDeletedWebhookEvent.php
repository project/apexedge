<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\BillerDeletedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when an Apexedge biller deleted.
 */
class BillerDeletedWebhookEvent extends Event {

  const BILLER_DELETED_WEBHOOK = 'apexedge_biller_deleted_webhook';

  /**
   * BillerDeletedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\BillerDeletedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\BillerDeletedPayload $payload
   *   BillerDeletedPayload payload.
   */
  public function __construct(BillerDeletedPayload $payload) {
    $this->payload = $payload;
  }

}
