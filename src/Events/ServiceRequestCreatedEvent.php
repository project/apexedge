<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\ServiceRequest\ServiceRequest;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Service request created.
 */
class ServiceRequestCreatedEvent extends Event {

  const SERVICE_REQUEST_CREATED = 'service_request_created';

  /**
   * ServiceRequest object.
   *
   * @var \Apexedge\DTO\ServiceRequest\ServiceRequest
   */
  public $serviceRequest;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\ServiceRequest\ServiceRequest $service_request
   *   ServiceRequest object.
   */
  public function __construct(ServiceRequest $service_request) {
    $this->serviceRequest = $service_request;
  }

}
