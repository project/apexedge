<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\BillerUpdatedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when an Apexedge biller updated.
 */
class BillerUpdatedWebhookEvent extends Event {

  const BILLER_UPDATED_WEBHOOK = 'apexedge_biller_updated_webhook';

  /**
   * BillerUpdatedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\BillerUpdatedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\BillerUpdatedPayload $payload
   *   BillerUpdatedPayload payload.
   */
  public function __construct(BillerUpdatedPayload $payload) {
    $this->payload = $payload;
  }

}
