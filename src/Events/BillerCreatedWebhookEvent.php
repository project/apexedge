<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\BillerCreatedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when an Apexedge biller created.
 */
class BillerCreatedWebhookEvent extends Event {

  const BILLER_CREATED_WEBHOOK = 'apexedge_biller_created_webhook';

  /**
   * BillerCreatedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\BillerCreatedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\BillerCreatedPayload $payload
   *   BillerCreatedPayload payload.
   */
  public function __construct(BillerCreatedPayload $payload) {
    $this->payload = $payload;
  }

}
