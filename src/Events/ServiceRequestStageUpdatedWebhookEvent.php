<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\ServiceRequestStageUpdatedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Service Request stage updated.
 */
class ServiceRequestStageUpdatedWebhookEvent extends Event {

  const SERVICE_REQUEST_STAGE_UPDATED_WEBHOOK = 'service_request_stage_updated_webhook';

  /**
   * ServiceRequestStageUpdatedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\ServiceRequestStageUpdatedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\ServiceRequestStageUpdatedPayload $payload
   *   ServiceRequestStageUpdatedPayload payload.
   */
  public function __construct(ServiceRequestStageUpdatedPayload $payload) {
    $this->payload = $payload;
  }

}
