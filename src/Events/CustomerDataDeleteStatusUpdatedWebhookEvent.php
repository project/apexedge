<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\CustomerDataDeleteStatusUpdatedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when Customer Data delete status updated.
 */
class CustomerDataDeleteStatusUpdatedWebhookEvent extends Event {

  const CUSTOMER_DATA_DELETE_STATUS_UPDATED_WEBHOOK = 'customer_data_delete_status_updated_webhook';

  /**
   * CustomerDataDeleteStatusUpdatedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\CustomerDataDeleteStatusUpdatedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\CustomerDataDeleteStatusUpdatedPayload $payload
   *   CustomerDataDeleteStatusUpdatedPayload payload.
   */
  public function __construct(CustomerDataDeleteStatusUpdatedPayload $payload) {
    $this->payload = $payload;
  }

}
