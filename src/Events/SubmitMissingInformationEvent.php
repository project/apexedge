<?php

namespace Drupal\apexedge\Events;

use Apexedge\ApexedgeResponse;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when submit missing information.
 */
class SubmitMissingInformationEvent extends Event {

  const SUBMIT_MISSING_INFORMATION = 'submit_missing_information';

  /**
   * Apexedge response.
   *
   * @var \Apexedge\ApexedgeResponse
   */
  public $response;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\ApexedgeResponse $response
   *   Apexedge response.
   */
  public function __construct(ApexedgeResponse $response) {
    $this->response = $response;
  }

}
