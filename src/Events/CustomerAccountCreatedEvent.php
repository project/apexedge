<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\ServiceRequest\CustomerAccount;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Customer Account created.
 */
class CustomerAccountCreatedEvent extends Event {

  const CUSTOMER_ACCOUNT_CREATED = 'customer_account_created';

  /**
   * CustomerAccount object.
   *
   * @var \Apexedge\DTO\ServiceRequest\CustomerAccount
   */
  public $account;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\ServiceRequest\CustomerAccount $account
   *   CustomerAccount object.
   */
  public function __construct(CustomerAccount $account) {
    $this->account = $account;
  }

}
