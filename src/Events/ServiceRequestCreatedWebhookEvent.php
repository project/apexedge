<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\ServiceRequestCreatedPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Service request created.
 */
class ServiceRequestCreatedWebhookEvent extends Event {

  const SERVICE_REQUEST_CREATED_WEBHOOK = 'service_request_created_webhook';

  /**
   * ServiceRequestCreatedPayload payload.
   *
   * @var \Apexedge\DTO\Payload\ServiceRequestCreatedPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\ServiceRequestCreatedPayload $payload
   *   ServiceRequestCreatedPayload payload.
   */
  public function __construct(ServiceRequestCreatedPayload $payload) {
    $this->payload = $payload;
  }

}
