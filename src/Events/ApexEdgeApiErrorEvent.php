<?php

namespace Drupal\apexedge\Events;

use Apexedge\Requests\Exceptions\InvalidApexedgeRequest;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when we get Invalid Apexedge Request.
 */
class ApexEdgeApiErrorEvent extends Event {

  const APEXEDGE_API_ERROR = 'apexedge_api_error';

  /**
   * Invalid Apexedge Request.
   *
   * @var \Apexedge\Requests\Exceptions\InvalidApexedgeRequest
   */
  public $request;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\Requests\Exceptions\InvalidApexedgeRequest $request
   *   Invalid Apexedge Request.
   */
  public function __construct(InvalidApexedgeRequest $request) {
    $this->request = $request;
  }

}
