<?php

namespace Drupal\apexedge\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Service request cancelled.
 */
class ServiceRequestCancelledEvent extends Event {

  const SERVICE_REQUEST_CANCELLED = 'service_request_cancelled';

  /**
   * Service request id.
   *
   * @var string
   */
  public $serviceRequestId;

  /**
   * Constructs the object.
   *
   * @param string $service_request_id
   *   Service request id.
   */
  public function __construct(string $service_request_id) {
    $this->serviceRequestId = $service_request_id;
  }

}
