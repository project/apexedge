<?php

namespace Drupal\apexedge\Events;

use Apexedge\DTO\Payload\ServiceRequestSavingsPromotionExpiredPayload;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a Service request savings promotion expired.
 */
class ServiceRequestSavingsPromotionExpiredWebhookEvent extends Event {

  const SERVICE_REQUEST_SAVINGS_PROMOTION_EXPIRED_WEBHOOK = 'service_request_savings_promotion_expired_webhook';

  /**
   * ServiceRequestSavingsPromotionExpiredPayload payload.
   *
   * @var \Apexedge\DTO\Payload\ServiceRequestSavingsPromotionExpiredPayload
   */
  public $payload;

  /**
   * Constructs the object.
   *
   * @param \Apexedge\DTO\Payload\ServiceRequestSavingsPromotionExpiredPayload $payload
   *   ServiceRequestSavingsPromotionExpiredPayload payload.
   */
  public function __construct(ServiceRequestSavingsPromotionExpiredPayload $payload) {
    $this->payload = $payload;
  }

}
