<?php

namespace Drupal\apexedge\Controller;

use Apexedge\ApexedgeWebhookParser;
use Apexedge\DTO\Payload\Payload;
use Apexedge\DTO\Webhook\Events\EnabledEvents;
use Drupal\apexedge\Events\BillerCreatedWebhookEvent;
use Drupal\apexedge\Events\BillerDeletedWebhookEvent;
use Drupal\apexedge\Events\BillerUpdatedWebhookEvent;
use Drupal\apexedge\Events\CustomerDataDeleteStatusUpdatedWebhookEvent;
use Drupal\apexedge\Events\ServiceRequestCreatedWebhookEvent;
use Drupal\apexedge\Events\ServiceRequestSavingsPromotionExpiredWebhookEvent;
use Drupal\apexedge\Events\ServiceRequestStageUpdatedWebhookEvent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Page for catch Apexedge webhooks.
 */
class ApexedgeWebhookController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   *   The HTTP request object.
   */
  protected $request;

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Drupal Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * ApexedgeWebhookController constructs.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance to use for configuration events.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger.
   */
  public function __construct(Request $request, EventDispatcherInterface $event_dispatcher, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->request = $request;
    $this->eventDispatcher = $event_dispatcher;
    $this->loggerChannelFactory = $logger_channel_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('event_dispatcher'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Listens for webhook notifications.
   */
  public function listener(Request $request) {
    $response = new Response();
    $webhook = new Payload(json_decode($request->getContent(), TRUE));
    try {
      $parsed_webhook = ApexedgeWebhookParser::parse($webhook);
      match ($webhook->event) {
        EnabledEvents::SERVICE_REQUEST_CREATED => $this->eventDispatcher->dispatch(new ServiceRequestCreatedWebhookEvent($parsed_webhook), ServiceRequestCreatedWebhookEvent::SERVICE_REQUEST_CREATED_WEBHOOK),
        EnabledEvents::SERVICE_REQUEST_STAGE_UPDATED => $this->eventDispatcher->dispatch(new ServiceRequestStageUpdatedWebhookEvent($parsed_webhook), ServiceRequestStageUpdatedWebhookEvent::SERVICE_REQUEST_STAGE_UPDATED_WEBHOOK),
        EnabledEvents::BILLER_CREATED => $this->eventDispatcher->dispatch(new BillerCreatedWebhookEvent($parsed_webhook), BillerCreatedWebhookEvent::BILLER_CREATED_WEBHOOK),
        EnabledEvents::BILLER_UPDATED => $this->eventDispatcher->dispatch(new BillerUpdatedWebhookEvent($parsed_webhook), BillerUpdatedWebhookEvent::BILLER_UPDATED_WEBHOOK),
        EnabledEvents::BILLER_DELETED => $this->eventDispatcher->dispatch(new BillerDeletedWebhookEvent($parsed_webhook), BillerDeletedWebhookEvent::BILLER_DELETED_WEBHOOK),
        EnabledEvents::CUSTOMER_DATA_DELETE_STATUS_UPDATED => $this->eventDispatcher->dispatch(new CustomerDataDeleteStatusUpdatedWebhookEvent($parsed_webhook), CustomerDataDeleteStatusUpdatedWebhookEvent::CUSTOMER_DATA_DELETE_STATUS_UPDATED_WEBHOOK),
        EnabledEvents::SERVICE_REQUEST_SAVINGS_PROMOTIONS_EXPIRED => $this->eventDispatcher->dispatch(new ServiceRequestSavingsPromotionExpiredWebhookEvent($parsed_webhook), ServiceRequestSavingsPromotionExpiredWebhookEvent::SERVICE_REQUEST_SAVINGS_PROMOTION_EXPIRED_WEBHOOK),
      };
    }
    catch (\InvalidArgumentException $e) {
      $this->loggerChannelFactory->get('apexedge')->error($e->getMessage());

      $this->loggerChannelFactory
        ->get('apexedge')
        ->error("
          Unable to parse Apexedge webhook:<br/>
          <strong>Error message: </strong>" . $e->getMessage() . "<br/>
          <strong>Conversation id: </strong>" . $webhook->conversation_id . "<br/>
          <strong>Customer id: </strong>" . $webhook->customer_id . "<br/>
          <strong>Event: </strong>" . $webhook->event->name . "<br/>
          <strong>Webhook id: </strong>" . $webhook->webhook->id . "<br/>
        ");

      return $response->setStatusCode(403);
    }

    return $response;
  }

}
