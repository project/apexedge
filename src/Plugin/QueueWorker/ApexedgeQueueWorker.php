<?php

namespace Drupal\apexedge\Plugin\QueueWorker;

use Drupal\apexedge\Services\BillerService;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync Apexedge billers.
 *
 * @QueueWorker(
 *   id = "apexedge_billers_sync_worker",
 *   title = @Translation("Apexedge billers sync worker"),
 *   cron = {"time" = 60}
 * )
 */
class ApexedgeQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Biller service.
   *
   * @var \Drupal\apexedge\Services\BillerService
   */
  protected $billerService;

  /**
   * Constructs a new AccessPerksWorker instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BillerService $biller_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->billerService = $biller_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('apexedge.biller_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->billerService->syncBillers();
  }

}
